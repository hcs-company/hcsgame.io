function Score(gameHeight,groundHeight,crateHeight) {
  this.score = 0;
  this.gameHeight = gameHeight;
  this.groundHeight = groundHeight;
  this.crateHeight = crateHeight;
  this.check = [gameHeight,groundHeight,crateHeight];

  this.addPoints = function(pos) {
    this.check.push(pos);
    var points = Math.round((this.gameHeight - this.groundHeight - pos - this.crateHeight/2)/this.crateHeight)+1;
    this.score += points;
    this.check.push(this.calcHash(pos,points));
    return points;
  }
  this.getScore = function() {
    return this.score;
  }
  this.calcHash = function(pos,points) {
    return Math.round(pos*this.groundHeight/this.crateHeight);
  }
  this.getCheck = function() {
    var res = this.getScore().toString(15);
    this.check.forEach( function(val) {
      res = res+"f"+val.toString(15);
    });
    return res;
  }
}
