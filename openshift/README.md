# OpenShift Deployment

This document describes how to deploy the demo on OpenShift manually.

## Introduction

The project can be deployed on OpenShift by simply processing the `kustomization.yml`
file. First create the desired namespace, the desired Secret and then apply the files using Kustomize.
The additional required services are deployed uponprocessing the files 
through Kustomization.

! The following section only applies when the `game` namespace will not be used !
! Pay attention inside the `kustomization.yml` to the `namespace`, `images` and `patches` section !

1. `namespace`
The value in the `namespace` field must be changed whenever a different namespace must be used.
Example: `namespace: game` to `namespace: <desired>` where <desired> is the desired namespace.

2. `images`
These 4 defined images will be gathered from the `game` namespace by default.
Whenever a different namespace is used or desired, these namespace values *must* be changed.
Example:
Change this `newName: image-registry.openshift-image-registry.svc:5000/game/game` to
`newName: image-registry.openshift-image-registry.svc:5000/<desired>/game` where <desired> is the desired namespace.

3. `patches`
The same applies for the `nodejs-imagestream-patch.yml` file. The last line inside this file must be changed to the desired namespace.
This can be done by chaning the `namespace: game` to `namespace: <desired>` where <desired> is the desired namespace.

## Deploy

```bash
oc new-project game
oc create secret generic postgresql --from-literal=database-name=highscore --from-literal=database-password=c-krit --from-literal=database-user=highscore
oc apply -k kustomization
```

Optionally, you can also rollout the dashboard. Sadly this doesn't work with the
current Python version due to version changes since 2018.

```bash
oc apply -f assets/dashboard.yml
```

## Scaling the deployment

The frontend and highscore services are stateless services and can be scaled
up "indefinitely". The backend service, kafka/zookeeper, redis and postgres are
not configured as a clustered setup and therefor are not scaleable. Increasing
the number of replicas for these services will fail, but will not impact the
current deployment.
