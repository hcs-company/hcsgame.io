---
kind: Service
apiVersion: v1
metadata:
  name: "game"
  annotations:
    description: Exposes and load balances the application pods
    prometheus.io/scrape: "true"
  labels:
    app: "game"
spec:
  ports:
  - name: http
    port: 8080
    targetPort: 8080
  selector:
    app: "game"
---
kind: Service
apiVersion: v1
metadata:
  name: "game-redis"
  annotations:
    description: Exposes and load balances the redis pods
  labels:
    app: "game"
spec:
  ports:
  - name: redis
    port: 6379
    targetPort: 6379
  selector:
    app: "game-redis"
---
kind: Route
apiVersion: route.openshift.io/v1
metadata:
  name: "game"
  labels:
    app: "game"
spec:
  to:
    kind: Service
    name: "game"
  tls:
    termination: edge
---
kind: ImageStream
apiVersion: image.openshift.io/v1
metadata:
  name: "game"
  annotations:
    description: Keeps track of changes in the application image
  labels:
    app: "game"
---
kind: BuildConfig
apiVersion: build.openshift.io/v1
metadata:
  name: "game"
  annotations:
    description: Defines how to build the application
  labels:
    app: "game"
spec:
  source:
    type: Git
    git:
      uri: "https://gitlab.com/hcscompany/hcsgame.io.git"
      ref: "master"
    contextDir: "game"
  strategy:
    type: Source
    sourceStrategy:
      from:
        kind: ImageStreamTag
        name: nodejs-8-rhel7:12
        namespace: openshift
  output:
    to:
      kind: ImageStreamTag
      name: "game:latest"
  triggers:
  - type: ConfigChange
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: "game"
  annotations:
    description: Defines how to deploy the application server
  labels:
    app: "game"
spec:
  triggers:
  - type: ImageChange
    imageChangeParams:
      automatic: true
      containerNames:
      - game
      from:
        kind: ImageStreamTag
        name: game:latest
  replicas: 1
  selector:
    matchLabels:
      app: "game"
  template:
    metadata:
      name: "game"
      labels:
        app: "game"
    spec:
      containers:
      - name: game
        image: game:latest
        imagePullPolicy: Always
        env:
        - name: "KAFKA_SERVERS"
          value: "kafka-kafka:9092"
        - name: "REDIS_HOST"
          value: "game-redis"
        ports:
        - containerPort: 8080
        readinessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 3
          httpGet:
            path: "/healthz"
            port: 8080
        livenessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 30
          httpGet:
            path: "/healthz"
            port: 8080
        resources:
          limits:
            memory: "256Mi"
          requests:
            cpu: "1"
---
kind: HorizontalPodAutoscaler
apiVersion: autoscaling/v1
metadata:
  labels:
    app: game
  name: game
spec:
  maxReplicas: 10
  minReplicas: 2
  scaleTargetRef:
    apiVersion: extensions/v1beta1
    kind: Deployment
    name: game
  targetCPUUtilizationPercentage: 70
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: "game-redis"
  annotations:
    description: Defines how to deploy the redis server
  labels:
    app: "game"
spec:
  replicas: 1
  selector:
    matchLabels:
      app: "game-redis"
  template:
    metadata:
      name: "game-redis"
      labels:
        app: "game-redis"
    spec:
      containers:
      - name: game
        image: docker.io/redis
        volumeMounts:
          - mountPath: /data
            name: redis-volume
        ports:
        - containerPort: 6379
        readinessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 3
          tcpSocket:
            port: 6379
        livenessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 30
          tcpSocket:
            port: 6379
        resources:
          limits:
            memory: "128Mi"
      volumes:
      - name: redis-volume
        emptyDir: {}
