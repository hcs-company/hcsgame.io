---
objects:
kind: Service
apiVersion: v1
metadata:
  name: "highscore"
  annotations:
    description: Exposes and load balances the application pods
    prometheus.io/scrape: "true"
    prometheus.io/path: /actuator/prometheus
  labels:
    app: "highscore"
spec:
  ports:
  - name: http
    port: 8080
    targetPort: 8080
  selector:
    app: "highscore"
---
kind: ImageStream
apiVersion: image.openshift.io/v1
metadata:
  name: "highscore"
  annotations:
    description: Keeps track of changes in the application image
  labels:
    app: "highscore"
---
kind: BuildConfig
apiVersion: build.openshift.io/v1
metadata:
  name: "highscore"
  annotations:
    description: Defines how to build the application
  labels:
    app: "highscore"
spec:
  source:
    type: Git
    git:
      uri: "https://gitlab.com/hcscompany/hcsgame.io.git"
      ref: "master"
    contextDir: "highscore"
  strategy:
    type: Source
    sourceStrategy:
      from:
        kind: ImageStreamTag
        name: redhat-openjdk18-openshift:1.3
        namespace: openshift
  output:
    to:
      kind: ImageStreamTag
      name: "highscore:latest"
  triggers:
  - type: ConfigChange
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: "highscore"
  annotations:
    description: Defines how to deploy the application server
  labels:
    app: "highscore"
spec:
  triggers:
  - type: ImageChange
    imageChangeParams:
      automatic: true
      containerNames:
      - highscore
      from:
        kind: ImageStreamTag
        name: highscore:latest
  replicas: 1
  selector:
      matchLabels:
        app: "highscore"
  template:
    metadata:
      name: "highscore"
      labels:
        app: "highscore"
    spec:
      containers:
      - name: highscore
        image: highscore:latest
        imagePullPolicy: Always
        env:
        - name: "SERVER_PORT"
          value: "8080"
        - name: "KAFKA_SERVERS"
          value: "kafka-kafka:9092"
        - name: "DATASOURCE_PLATFORM"
          value: "postgres"
        - name: "DATASOURCE_URL"
          value: "jdbc:postgresql://postgresql:5432/highscore"
        - name: "DATASOURCE_USER"
          valueFrom:
            secretKeyRef:
              name: "postgresql"
              key: database-user
        - name: "DATASOURCE_PASSWORD"
          valueFrom:
            secretKeyRef:
              name: "postgresql"
              key: database-password
        ports:
        - containerPort: 8080
        readinessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 3
          httpGet:
            path: "/actuator"
            port: 8080
        livenessProbe:
          timeoutSeconds: 3
          initialDelaySeconds: 30
          httpGet:
            path: "/actuator"
            port: 8080
        resources:
          limits:
            memory: "512Mi"
